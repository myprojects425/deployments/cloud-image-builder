terraform {
  required_providers {    
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}


data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_al"
  description = "Allow SSH inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh_image"
  }
}


data "aws_ami" "target" {
  filter {
    name   = "name"
    values = ["${var.target}-${var.target_version}-${var.linux_flavor}-*"]
  }
  owners = var.ami_owners
  most_recent = true
}

locals {
  subnet = tolist(data.aws_subnets.default.ids)[0]
}

resource "aws_instance" "instance" {
  ami             = data.aws_ami.target.image_id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.allow_ssh.id] # CREATE CUSTOM RULES
  subnet_id       = local.subnet
  key_name        = var.key_pair
  associate_public_ip_address = true

  tags = {
    Name = var.target
  }
}
