variable "aws_region" {
  type = string
  description = "AWS region"
}

variable "instance_type" {
  type = string
  default = "t3.micro"
  description = "AWS instance type"
}


variable "key_pair" {
  type = string
  description = "Key Pair"
}

# AMI variables

variable "target" {
  type = string
  description = "Image target (possible values: hadoop, zookeeper)"
}

variable "target_version" {
  type = string
  description = "Image target version (possible values: hadoop = [3.3.6, ], zookeeper = [3.9.1, ])"
}

variable "linux_flavor" {
  type    = string
  default = "ubuntu22.04" #local.default_linux_flavor

  validation {
    condition = contains(["ubuntu20.04", "ubuntu22.04"], var.linux_flavor) # local.available_linux_flavors
    error_message = "Valid values for linux_flavor variable: ['ubuntu20.04', 'ubuntu22.04']"
  }
}

variable "ami_owners" {
  type = list(string)
  default = ["811275715575"] # glozanoac
  description = "Trusted owners of AMI"
}
