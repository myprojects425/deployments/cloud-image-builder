IMPORTANT: Change the ami owner in `terraform.tfvars` variables file.

* Get AMI owner
AWS Console -> EC2 -> Images (AMIs)
![ami-owner](./images/ami_owner.png)

