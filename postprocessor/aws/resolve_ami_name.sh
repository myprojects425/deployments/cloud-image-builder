#!/bin/bash
#
# Resolve the AMI name into the AMI id

if [[ $# -lt 1 ]];then
    echo "USAGE: $0 <AMI_NAME> <REGION>"
    exit 1
fi

AMI_NAME=$1
REGION=$2

ami=$(aws ec2 describe-images \
          --filter="Name=name,Values=$AMI_NAME" \
          --region $REGION \
          --query "Images[].ImageId" \
          --output text
   )
