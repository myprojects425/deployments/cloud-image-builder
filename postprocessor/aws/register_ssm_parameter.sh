#!/bin/bash
#
# Register an AMI as a ssm parameter

if [[ $# -lt 2 ]];then
    echo """
    USAGE: $0 <PARAMETER_NAME> <AMI>

    EXAMPLE:

    $0 /hadoop/3.3.6/ubuntu-20.04-20230760 ami-1234567789
    """
    exit 1
fi

PARAMETER_NAME=$1
AMI=$2

aws ssm put-parameter --name $PARAMETER_NAME --type String --value $AMI
