Create custom AMI by applying a role to a base image.

```txt
# Deploying complex systems
1. BASE_IMAGE --APPLY ROLE--> CUSTOM_IMAGE
2. CUSTOM_IMAGE --DEPLOY--> PROVISIONED INFRASTRUCTURE -- CONFIGURE --> CONFIGURED INFRASTRUCTURE
```
